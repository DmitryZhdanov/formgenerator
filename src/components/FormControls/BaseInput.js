import React from 'react'

export default ({ value, onChange, type = 'text', ...rest }) => {

    const handleChange = e => onChange(e.target.value)
    
    return (
        <input
            type={type}
            value={value}
            onChange={handleChange}
            className="input" {...rest}
        />
    )
}