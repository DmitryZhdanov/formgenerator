import React, { useState, Suspense } from 'react'

import { useLazyComponent } from '../hooks'

import * as R from 'ramda'

import { validators, castMethod, expressionRegexp } from '../utils'
import LazyControl from './LazyControl'

export default ({ scheme, onSubmit }) => {
    const { fields } = scheme
    const [components, ready] = useLazyComponent(fields.map(R.prop('control')))
    const [submited, setSubmited] = useState(false)

    const [form, updateForm] = useState(
        fields.reduce((p, n) => {
            return {
                ...p,
                [n.key]: {
                    value: '',
                    error: false
                }
            }
        }, {})
    )

    const setValue = key => value =>
        updateForm(state => {
            const object = Object.assign({}, form[key])

            object.value = value

            return ({...state, [key]: object })
        })

    const validateField = ({ key, rules }) => {
        let error = false

        rules.some(({ validator, when, message, type }) => {

            const validate = expression => {
                const [, fn, dep, ar, target ] = expressionRegexp.exec(expression)
                const { value } = R.prop(dep || key, form)

                return validators[fn](ar, target)(value)
            }

            const checkValidity = () => {
                if(R.not(validator[castMethod(type)](validate))) {
                    error = message
                }
            }


            if(!when) {
                return checkValidity()
            }

            return validate(when)
                ? checkValidity()
                : false
        })
        
        if(form[key].error !== error) {
            updateForm(form => {
                const object = Object.assign({}, form[key])
    
                object.error = error
    
                return {
                    ...form,
                    [key]: object
                }
            })
        }
    }

    const isFormValid = Object.keys(form).every(key => form[key].error === false)

    const submit = (e) => {
        e.preventDefault()

        if(isFormValid) {
            onSubmit(form)
        }

        if(!submited) {
            setSubmited(true)
        }
    }
    

    const fieldRenderer = (field, i) => {
        const { control, label, placeholder } = field
        const Control = components[control]

        validateField(field)

        return (
            <LazyControl
                key={i}
                label={label}
                error={submited && form[field.key].error}
            >
                <Control
                    placeholder={placeholder}
                    value={form[field.key].value}
                    onChange={setValue(field.key)}    
                />
            </LazyControl>
        )
    }

    if(!ready) {
        return <div>loading</div>
    }

    return (
        <form onSubmit={submit} className="form">
            {fields.map(fieldRenderer)}

            <button disabled={submited && !isFormValid} className="form-submit">Сохранить</button>
        </form>
    )
}