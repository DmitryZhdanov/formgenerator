import React, { Suspense } from 'react'

export default ({ error, children, label }) => {

    const classNames = ['control-content', error && '-has-error'].filter(Boolean).join(' ')
    console.log(classNames);
    
    return (
        <div className="control">
            <label className="control-label">{label}</label>

            <div className={classNames}>
                <Suspense fallback={<div>loading control...</div>}>
                    {children}
                    {error && <div className="control-error">{error}</div>}
                </Suspense>
            </div>
        </div>
    )
}