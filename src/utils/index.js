import * as R from 'ramda'

export const expressionRegexp = /([a-z]+)(?::([a-z]+))?:?([<>=])?(\d+)?/i

export const compare = outer => R.cond([
    [R.equals('='), (_, v) => R.equals(v, +outer)],
    [R.equals('<'), (_, v) => R.gt(v, +outer)],
    [R.equals('>'), (_, v) => R.lt(v, +outer)],
])

export const castMethod = R.cond([
    [R.equals('or'), R.always('some')],
    [R.equals('and'), R.always('every')],
    
    [R.T, R.always('some')],
])

export const validators = {
    required: () => value => !!value.toString().trim(),
    length: (ar, target) => value => {
        const length = value.toString().trim().length

        return compare(length)(ar, +target)
    } 
}
