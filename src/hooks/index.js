import { useState, useRef, useEffect, lazy } from 'react'

export const useLazyComponent = (controls) => {
    const components = useRef(null)
    const [ready, setReady] = useState(false)

    const loadComponents = () => {
        components.current = controls
            .reduce((p, n) => {
                return {
                    ...p,
                    [n]: lazy(() => import('../components/FormControls/' + n))
                }
            }, {})

        setReady(true)
    }

    useEffect(loadComponents, [])

    return [components.current, ready]
}