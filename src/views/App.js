import React from 'react';

import FormGenerator from '../components/FormGenerator'
import scheme from '../form.scheme'

function App() {
  const onSubmit = () => alert('submit')

  return (
    <div className="app">
      <FormGenerator scheme={scheme} onSubmit={onSubmit}/>
    </div>
  );
}

export default App;
